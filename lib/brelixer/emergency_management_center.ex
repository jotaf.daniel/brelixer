defmodule Brelixer.EmergencyManagementCenter do
  alias Brelixer.Broker

  def start do
    Broker.subscribe(:broker, self(), :rainwater_collection)

    handle_events()
  end

  defp handle_events do
    receive do
      msg ->
        handle_message(msg)
    end
  end

  defp handle_message({:rainwater_collection, data}) do
    if data > 120 do
      Broker.publish(:broker, :public_emergency, "risk of flood")
    end

    handle_events()
  end
end
