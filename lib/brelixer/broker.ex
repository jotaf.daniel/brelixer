defmodule Brelixer.Broker do
  def start do
    spawn(fn ->
      loop(%{})
    end)
  end

  def subscribe(broker_pid, client_pid, topic) do
    broker_pid
    |> send({:subscribe, client_pid, topic})
  end

  def publish(pid, topic, payload) do
    pid
    |> send({:publish, topic, payload})
  end

  defp loop(state) do
    new_state =
      receive do
        {:subscribe, client, topic} ->
          state
          |> Map.update(
            topic,
            [client],
            &[client | &1]
          )

        {:publish, topic, payload} ->
          IO.inspect({topic, payload})

          state
          |> Map.get(topic, [])
          |> Enum.each(&send(&1, {topic, payload}))

          state
      end

    loop(new_state)
  end
end
