defmodule Brelixer.Pluviometer do
  alias Brelixer.Broker

  def start do
    collect_data()
  end

  defp collect_data() do
    Process.sleep(500)

    data = :rand.uniform(200)

    Broker.publish(:broker, :rainwater_collection, data)

    collect_data()
  end
end
