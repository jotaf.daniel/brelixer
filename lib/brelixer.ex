defmodule Brelixer do
  alias Brelixer.{
    Broker,
    Pluviometer,
    EmergencyManagementCenter,
    PollutionMeter
  }

  @moduledoc """
  Documentation for `Brelixer`.
  """
  def start do
    Broker.start()
    |> Process.register(:broker)

    clients = [
      fn -> EmergencyManagementCenter.start() end,
      fn -> Pluviometer.start() end,
      fn -> PollutionMeter.start() end
    ]

    clients
    |> Enum.map(fn fun -> spawn(fun) end)

    hang()
  end

  defp hang() do
    receive do
      {:close} ->
        nil
    end
  end
end
